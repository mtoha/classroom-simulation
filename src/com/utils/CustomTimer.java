/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 *
 * @author ManInMirror-Laptop
 */
public class CustomTimer {

    private static int jam = 0, menit = 0, detik = 0;
    private static javax.swing.Timer myTimer;
    private static String buff = "";

    public static String startTimer() {
        myTimer = new Timer(1000, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                detik++;
                if (detik == 60) {
                    detik = 0;
                    menit++;
                }
                if (menit == 60) {
                    menit = 0;
                    jam++;
                }
                if (jam == 24) {
                    jam = 0;
                }
                buff = String.format(""+jam, menit, detik); // penampung string
            }
        });

        myTimer.start();
        return buff;
    }
}
