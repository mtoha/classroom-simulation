
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gui;

//~--- JDK imports ------------------------------------------------------------
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;
import javax.swing.Timer;

/**
 * An applet that counts down from a specified time. When it reaches 00:00, it
 * optionally plays a sound and optionally moves the browser to a new page.
 * Place the mouse over the applet to pause the count; move it off to resume.
 * This class demonstrates most applet methods and features.
 */
public class StopWatch {

    private long startTime = 10000;
    private long stopTime = 20;
    private boolean isStart = false;

    /**
     * Construct a new StopWatch object.
     */
    public StopWatch() {
    }

    /**
     * Start this StopWatch object. Starting a StopWatch that was already
     * running resets the watch.
     */
    public void start() {
        startTime = System.currentTimeMillis();
        isStart = true;
    }

    /**
     * Stop this StopWatch object.
     */
    public void stop() {
        stopTime = System.currentTimeMillis();
        isStart = false;
    }

    /**
     * Get the time recorded by this StopWatch object. The value returned if
     * getTime is called before this StopWatch has been stopped is meaningless.
     */
    public long getElapsedTime() {
        return (System.currentTimeMillis() - startTime);
    }

    public String getTime() {
        
        if (isStart) {            
            long elapsedTime = getElapsedTime();
            String format = String.format("%%0%dd", 2);
            elapsedTime = elapsedTime / 1000;
            String seconds = String.format(format, elapsedTime % 60);
            String minutes = String.format(format, (elapsedTime % 3600) / 60);
            String hours = String.format(format, elapsedTime / 3600);
            String time = hours + ":" + minutes + ":" + seconds;
            return time;
        } else {
            return "00:00";
        }
    }

    public boolean inPast(String t) {
        StringTokenizer st = new StringTokenizer(t, ":");
        //put all in array

        int tt[] = new int[st.countTokens()];
        int i = 0;
        while (st.hasMoreTokens()) {
            tt[i] = Integer.parseInt(st.nextToken());
            i++;
        }
        if (tt.length == 2) {
            //mm:ss
            return (tt[0] * 60 + tt[1]) * 1000 < getElapsedTime();
        } else if (tt.length == 3) {
            return ((tt[0] * 60 + tt[1]) * 60 + tt[2]) * 1000 < getElapsedTime();
        }
        return false;
    }
}
