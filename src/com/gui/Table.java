/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JTable;


public class Table extends JTable {

    String url = "jdbc:mysql://localhost:3306/";
    String dbName = "simulation";
    String driver = "com.mysql.jdbc.Driver";
    String userName = "root";
    String password = "123";
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private String id = "id";
    private String Facial = "Facial";
    private String Sitting = "Sitting";
    private String Reaction = "Reaction";
    private String Personality = "Personality";
    private String Strategy = "Strategy";
    private String Timer = "Timer";
     private String Sex = "Sex";
    ArrayList<dataBase> dataSet = new ArrayList<dataBase>();

   /* DefaultTableModel model = new DefaultTableModel(new Object [][] {},
    new String [] {
        "Id", "Facial", "Sitting", "Reaction", "Personality", "Strategy", "Timer"
    });*/

    public Table() {
        //JTable table = new JTable(model);
    }

    public void readDataBase() throws Exception {

        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName(driver).newInstance();
            // Setup the connection with the DB
            connect = DriverManager.getConnection(url + dbName, userName, password);
            //connect = DriverManager.getConnection("jdbc:mysql://localhost/feedback?"
            //					+ "user=root&password=123");
            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            resultSet = statement.executeQuery("select * from classroom");

            /*
            // PreparedStatements can use variables and are more efficient
            preparedStatement = connect.prepareStatement("insert into  FEEDBACK.COMMENTS values (default, ?, ?, ?, ? , ?, ?)");
            // "myuser, webpage, datum, summery, COMMENTS from FEEDBACK.COMMENTS");
            // Parameters start with 1
            preparedStatement.setString(1, "Test");
            preparedStatement.setString(2, "TestEmail");
            preparedStatement.setString(3, "TestWebpage");
            preparedStatement.setDate(4, new java.sql.Date(2009, 12, 11));
            preparedStatement.setString(5, "TestSummary");
            preparedStatement.setString(6, "TestComment");
            preparedStatement.executeUpdate();

            preparedStatement = connect.prepareStatement("SELECT myuser, webpage, datum, summery, COMMENTS from FEEDBACK.COMMENTS");
            resultSet = preparedStatement.executeQuery();
            writeResultSet(resultSet);

            // Remove again the insert comment
            preparedStatement = connect.prepareStatement("delete from FEEDBACK.COMMENTS where myuser= ? ; ");
            preparedStatement.setString(1, "Test");
            preparedStatement.executeUpdate();

            resultSet = statement.executeQuery("select * from FEEDBACK.COMMENTS");
             * 
             */
            writeMetaData(resultSet);
            writeResultSet(resultSet);
            PrintData();

        } catch (Exception e) {

            throw e;
        } finally {
            close();
        }


    }

    private void writeMetaData(ResultSet resultSet) throws SQLException {
        // 	Now get some metadata from the database
        // Result set get the result of the SQL query

        System.out.println("The columns in the table are: ");

        System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
        for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
            System.out.print(resultSet.getMetaData().getColumnName(i) + " ");
        }
        System.out.println();
    }

    private void writeResultSet(ResultSet resultSet) throws SQLException {
        // ResultSet is initially before the first data set
        dataSet.clear();
        while (resultSet.next()) {
            // It is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g. resultSet.getSTring(2);


            dataSet.add(new dataBase(resultSet.getInt(id),
                    resultSet.getString(Facial),
                    resultSet.getString(Sitting),
                    resultSet.getString(Reaction),
                    resultSet.getString(Personality),
                    resultSet.getString(Strategy),
                    resultSet.getString(Timer),
                    resultSet.getString(Sex)));
           

        }
    }

    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }

    public void PrintData() {
        for (int i = 0; i < dataSet.size(); i++) {
            System.out.println(dataSet.get(i).getId());
        }
    }
    /**
     * @param args the command line arguments
     */
    /*
    public static void main(String[] args) {
    // TODO code application logic here
    Main m = new Main();
    try{
    m.readDataBase();
    }catch(Exception e){
    System.out.println("Expection");
    System.out.println(e.getMessage());
    }
    }*/
}
