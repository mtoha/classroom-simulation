/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gui;

import java.awt.Image;
import java.awt.Point;

/**
 *
 * @author RATHI
 */
public class studen_icons {

    private Student student;
    private Point point;



    public studen_icons(Student student, Point point) {
        this.student = student;
        this.point = point;
    }

    public Image getImage() {
        return student.getImage();
    }

    public void setImage(Student student) {
        this.student = student;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    

    public int getX(){
        return this.point.x;
    }

    public int getY(){
        return this.point.y;
    }
}
