/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gui;


public class dataBase {

    private int id;
    private String Facial;
    private String Sitting;
    private String Reaction;
    private String Personality;
    private String Strategy;
    private String Timer;
    private String Sex;

    public String getSex() {
        return Sex;
    }

    public void setSex(String Sex) {
        this.Sex = Sex;
    }


    public String[] to_array() {
        String[] s = { Integer.toString(id), Facial, Sitting, Reaction, Personality, Strategy, Timer  };
        return s;
    }

    public dataBase(int id, String Facial, String Sitting, String Reaction, String Personality, String Strategy, String Timer, String Sex) {
        this.id = id;
        this.Facial = Facial;
        this.Sitting = Sitting;
        this.Reaction = Reaction;
        this.Personality = Personality;
        this.Strategy = Strategy;
        this.Timer = Timer;
        this.Sex = Sex;
    }

    

    public void setId(int id) {
        this.id = id;
    }

   

    public String getFacial() {
        return Facial;
    }

    public void setFacial(String Facial) {
        this.Facial = Facial;
    }

    public String getPersonality() {
        return Personality;
    }

    public void setPersonality(String Personality) {
        this.Personality = Personality;
    }

    public String getReaction() {
        return Reaction;
    }

    public void setReaction(String Reaction) {
        this.Reaction = Reaction;
    }

    public String getSitting() {
        return Sitting;
    }

    public void setSitting(String Sitting) {
        this.Sitting = Sitting;
    }

    public String getStrategy() {
        return Strategy;
    }

    public void setStrategy(String Strategy) {
        this.Strategy = Strategy;
    }

    public String getTimer() {
        return Timer;
    }

    public void setTimer(String Timer) {
        this.Timer = Timer;
    }

    public int getId() {
        return id;
    }

   



}
