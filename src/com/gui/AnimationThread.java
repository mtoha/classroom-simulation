/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gui;


public class AnimationThread extends Thread{

    ImagePanel animationApplet;
    int delay;
    public boolean stopped = false;

    

    public AnimationThread(ImagePanel a, int delay) {
        this.animationApplet = a;
        this.delay = delay;
    }

    @Override
    public void run() {
        while (!stopped) {
            try {
                if (animationApplet.isempty()) {
                   stopped = true;
                    
                } else {
                    animationApplet.animate();
                    Thread.sleep(delay);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
    }
}
