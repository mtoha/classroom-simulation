/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gui;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author RATHI
 */
public class Student {

    private String sex;
    private String Facial;
    private String Sitting;
    private String Reaction;
    private String Personality;
    private String Strategy;
    private int id;
    private String Timer;

    public String getTimer() {
        return Timer;
    }

    public void setTimer(String Timer) {
        this.Timer = Timer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student(String sex, String Facial, String Sitting, String Reaction, String Personality, String Strategy, int id, String Timer) {
        this.sex = sex;
        this.Facial = Facial;
        this.Sitting = Sitting;
        this.Reaction = Reaction;
        this.Personality = Personality;
        this.Strategy = Strategy;
        this.id = id;
        this.Timer = Timer;
    }

    

    public String getFacial() {
        return Facial;
    }

    public void setFacial(String Facial) {
        this.Facial = Facial;
    }

    public String getPersonality() {
        return Personality;
    }

    public void setPersonality(String Personality) {
        this.Personality = Personality;
    }

    public String getReaction() {
        return Reaction;
    }

    public void setReaction(String Reaction) {
        this.Reaction = Reaction;
    }

    public String getSitting() {
        return Sitting;
    }

    public void setSitting(String Sitting) {
        this.Sitting = Sitting;
    }

    public String getStrategy() {
        return Strategy;
    }

    public void setStrategy(String Strategy) {
        this.Strategy = Strategy;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Image getImage() {
        
        String s = null;
        if(this.Facial.equalsIgnoreCase("Happy")){
            s = "4";
        }else if(this.Facial.equalsIgnoreCase("Sad")){

            s = "2";
        }else if(this.Facial.equalsIgnoreCase("confuse")){
            s = "3";
        }else if(this.Facial.equalsIgnoreCase("Angry")){
            s = "1";
        }
        
        if (this.sex.equals("F")) {

            return new ImageIcon("src/img/girl" + s +".png").getImage();
            //return new ImageIcon("src/img/girl.png").getImage();
        } else {
            return new ImageIcon("src/img/boy" + s +".png").getImage();
            //return new ImageIcon("src/img/boy.png").getImage();
        }


    }
}
