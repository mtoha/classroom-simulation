/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class ImagePanel extends JPanel implements ActionListener {

    private Image background = new ImageIcon("src/img/back_small.png").getImage();
    private Image teachersTable = new ImageIcon("src/img/meja-guru.png").getImage();
    //Computer-table
    private Image computerTable = new ImageIcon("src/img/Computer-table.png").getImage();
    //student image
    private Image studentImg = new ImageIcon("src/img/boy4.png").getImage();
    // private ArrayList<Student> list = new ArrayList<Student>();
    private HashMap<Integer, ArrayList<Student>> slist = new HashMap<Integer, ArrayList<Student>>();
//    StopWatch watch = new StopWatch();
    private Timer timer;
    private int jam = 0, menit = 0, detik = 0;
    private Point points[] = {
        new Point(270, 160), new Point(270, 240), new Point(270, 320), new Point(270, 400),
        new Point(270, 480), new Point(270, 560), new Point(360, 160), new Point(360, 240),
        new Point(360, 320), new Point(360, 400), new Point(360, 480), new Point(360, 560),
        new Point(450, 160), new Point(450, 240), new Point(450, 320), new Point(450, 400),
        new Point(450, 480), new Point(450, 560), new Point(540, 160), new Point(540, 240),
        new Point(540, 320), new Point(540, 400), new Point(540, 480), new Point(540, 560)
    };

    public ImagePanel() {
        setDoubleBuffered(true);
        timer = new Timer(1000, this);
        timer.start();
        //this.background = img;
        //Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
        Dimension size = new Dimension(background.getWidth(null), background.getHeight(null));
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        setSize(size);
        setLayout(null);
    }

    @Override
    @SuppressWarnings("element-type-mismatch")
    public void paint(Graphics g) {

        super.paintComponent(g);


        //draw bacground
        g.drawImage(this.background, 0, 0, null);
        //draw teachers table

        double w = 0.4 * teachersTable.getWidth(this);
        double h = 0.4 * teachersTable.getHeight(this);
        g.drawImage(this.teachersTable, 60, 200, (int) w, (int) h, this);

        //draw computer tables

        w = 0.4 * computerTable.getWidth(this);
        h = 0.4 * computerTable.getHeight(this);
        g.drawImage(this.computerTable, 540, 200, (int) w, (int) h, this);
        g.drawImage(this.computerTable, 560, 308, (int) w, (int) h, this);

        //draw Timer

        g.drawRoundRect(630, 550, 100, 50, 15, 15);
        g.fillRoundRect(630, 550, 100, 50, 15, 15);
        g.setColor(Color.WHITE);
        g.drawString("" + jam + ":" + menit + ":" + detik, 630 + 30, 550 + 30);
        g.setColor(Color.BLACK);

        if (slist.isEmpty()) {
            for (int i = 0; i < 24; i++) {
                g.drawImage(studentImg, this.points[i].x, this.points[i].y,
                        (int) (0.25 * studentImg.getWidth(this)),
                        (int) (0.25 * studentImg.getHeight(this)),
                        this);
            }
        }

        //check here for entry for past
        for (int i = 1; i < 25; i++) {
            if (slist.containsKey(i)) {
                if (slist.get(i).isEmpty()) {
                    System.out.println("Student ID : " + i + " NO Data");
                    slist.remove(i);
                } else {
//                    if (watch.inPast(slist.get(i).get(0).getTimer())) {
//                        //head of studentList's is  in past
//                        slist.get(i).remove(0);
//                    }
                }
            }
        }

        for (int i = 1; i < 25; i++) {


            if (slist.containsKey(i)) {
                try {
                    g.drawImage(slist.get(i).get(0).getImage(), this.points[i - 1].x, this.points[i - 1].y,
                            (int) (0.25 * slist.get(i).get(0).getImage().getWidth(this)),
                            (int) (0.25 * slist.get(i).get(0).getImage().getHeight(this)),
                            this);

                    if (!slist.get(i).get(0).getFacial().equalsIgnoreCase("Happy")) {
                        g.drawRoundRect(this.points[i - 1].x + 30, this.points[i - 1].y - 30, 80, 30, 15, 15);
                        g.setColor(Color.LIGHT_GRAY);
                        g.fillRoundRect(this.points[i - 1].x + 30, this.points[i - 1].y - 30, 80, 30, 15, 15);
                        g.setColor(Color.BLACK);
                        g.drawString(slist.get(i).get(0).getFacial(), this.points[i - 1].x + 35, this.points[i - 1].y - 20);
                        g.drawString(slist.get(i).get(0).getSitting(), this.points[i - 1].x + 35, this.points[i - 1].y - 5);
                    }
                } catch (Exception e) {
                    System.out.println("key is : " + i);
                    System.out.println(e.getMessage());

                }

            } else {
                g.drawImage(studentImg, this.points[i - 1].x, this.points[i - 1].y,
                        (int) (0.25 * studentImg.getWidth(this)),
                        (int) (0.25 * studentImg.getHeight(this)),
                        this);

                g.drawRoundRect(this.points[i - 1].x + 30, this.points[i - 1].y - 30, 80, 30, 15, 15);
                g.setColor(Color.LIGHT_GRAY);
                g.fillRoundRect(this.points[i - 1].x + 30, this.points[i - 1].y - 30, 80, 30, 15, 15);
                g.setColor(Color.BLACK);

                g.drawString(" ", this.points[i - 1].x + 35, this.points[i - 1].y - 5);


                // g.drawString("No Data123", this.points[i - 1].x + 35, this.points[i - 1].y - 5);
            }

            /*
             * g.drawImage(list.get(i).getImage(), this.points[i].x,
             * this.points[i].y, (int) (0.25 *
             * list.get(i).getImage().getWidth(this)), (int) (0.25 *
             * list.get(i).getImage().getHeight(this)), this);
             */
            //draw text on top

        }
//        super.paint(g);
//        Graphics2D g2d = (Graphics2D) g;
        Toolkit.getDefaultToolkit().sync();
        g.dispose();


    }

    public void animate() {
        repaint();

    }

    public boolean isempty() {
        return slist.isEmpty();
    }

    public void addTolist(Student student) {
        if (slist.containsKey(student.getId())) {
            slist.get(student.getId()).add(student);
        } else {
            //new student
            ArrayList<Student> studentList = new ArrayList<Student>();
            studentList.add(student);
            slist.put(student.getId(), studentList);
        }

    }
    /*
     * public void run() {
     *
     * Thread me = Thread.currentThread(); while (thread == me) { try {
     * repaint(); thread.sleep(999); } catch (Exception e) {
     * e.printStackTrace(); } } thread = null; }
     */
    // @Override
    /*
     * public void start() { thread = new Thread(this);
     * thread.setPriority(Thread.MIN_PRIORITY); thread.start(); }
     *
     * public void stop() { if (thread != null) { thread.interrupt(); } thread =
     * null; }
     */

    public void actionPerformed(ActionEvent e) {
        detik++;
        if (detik == 60) {
            detik = 0;
            menit++;
        }
        if (menit == 60) {
            menit = 0;
            jam++;
        }
        if (jam == 24) {
            jam = 0;
        }
        repaint();
    }
}
